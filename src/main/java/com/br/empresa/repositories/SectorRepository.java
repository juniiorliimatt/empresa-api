package com.br.empresa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.empresa.entities.Sector;

@Repository
public interface SectorRepository extends JpaRepository<Sector, Long>{

}
